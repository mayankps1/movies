this Vue.js application is a movie website that displays a list of movies and allows users to search for movies. The website has several components, including MovieCard, NavBar, and HomeView.

The MovieCard component is a reusable card that displays movie information, including the movie's title, poster, overview, release date, and genres. It uses Bootstrap's b-card component and truncates the movie's overview to 150 characters. It also includes a button to bookmark the movie and genre buttons to display the movie's genres.

The NavBar component is the navigation bar at the top of the website. It includes links to the Home, About, and Favourite pages and a search bar to search for movies.

The HomeView component is the main page that displays a list of movies using the MovieCard component. It includes pagination to display a certain number of movies per page and allows users to navigate to different pages of the movie list.

Finally, the App component is the main Vue.js component that includes the NavBar component and renders the router-view component to display different pages depending on the current route.

# movie

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
